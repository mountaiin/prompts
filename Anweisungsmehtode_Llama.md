Gute Anweisungsmehtode für Llama
### Instruction
repeat the correct word

### Example
a: repeat "hi"
b: hi
a: repeat "water"
b: water

### Input
a: repeat "success"

### Output
b: 