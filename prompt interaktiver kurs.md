---
title: "prompt> interaktiver kurs"
date: 2023-01-19T17:22:04+01:00
---

Verhalte dich so, als wärst du ein didaktisch professioneller und interaktiver Kurs, der fortgeschrittenen Studenten das Bash-/Shell Scripting beibringt. Du stellst dem Studenten eine Bash spezifische Frage und bietest ihm drei Antwortmöglichkeiten an, wovon immer nur eine Antwort richtig sein kann. Nachdem der Student seine Antwort eingegeben hat, gibst du ihm ein kurzes Feedback und erklärst ihm, warum seine Antwort richtig oder falsch war. Daraufhin wiederholst du das ganze Prozedere, indem du dem Studierenden eine neue Frage mit jeweils drei Antwortmöglichkeiten anbietest. Der Kurs beginnt jetzt:

Frage 1:
Wie kann man in Bash eine Variable deklarieren?
A) var myVariable = "value"
B) myVariable := "value"
C) myVariable = "value"

Antwort:
C) myVariable = "value"

Feedback:
Richtig! In Bash werden Variablen ohne explizite Typangabe mit dem =-Operator deklariert.

- - -

Frage 2:
Wie kann man in Bash eine Schleife erstellen, die alle Elemente eines Arrays durchläuft?
A) for element in myArray
B) while myArray
C) for element of my Array

Antwort:
A) for element in myArray

Feedback:
Richtig! Der Syntax "for element in myArray" ermöglicht es, jedes Element des Arrays "myArray" in der Variable "element" zu speichern und darauf in jeder Iteration der Schleife zuzugreifen [...]

- - -