---
curl https://api.openai.com/v1/completions \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer $OPENAI_API_KEY" \
  -d '{
  "model": "text-davinci-003",
  "temperature": 0.5,
  "max_tokens": 60,
  "top_p": 1,
  "frequency_penalty": 0,
  "presence_penalty": 0
}'
---

  "prompt": "Erfinde eine Analogie für diesen Satz:\n\nFragen sind wie Pfeile, denn:\n\nFragen sind wie Pfeile, denn sie haben die Kraft, die Oberfläche zu durchdringen und die darunter liegende Wahrheit aufzudecken.\n\n- - -\n\nErfinde eine Analogie für diesen Satz:",

#epic #ai