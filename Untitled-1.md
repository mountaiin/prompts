---
curl https://api.openai.com/v1/completions \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer $OPENAI_API_KEY" \
  -d '{
  "model": "code-davinci-002",
  "prompt": "",
  "temperature": 1,
  "max_tokens": 525,
  "top_p": 0.9,
  "frequency_penalty": 0.97,
  "presence_penalty": 2,
  "stop": ["exit", "Yazan:"]
}'
---
